import tensorflow as tf
import os


os.environ["CUDA_VISIBLE_DEVICES"]="-1"
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))