import tensorflow as tf
import keras_preprocessing.image as keras_img
import numpy as np
import glob
import pandas as pd
import os
import gc
import argparse



def load_model(model_dir):
    """Load the xception model. This function is looking for the directory with the .pb file"""
    print("Loading model...")
    return tf.keras.models.load_model(model_dir)


def prep_image(fname):
    """Load and prep images for model, reshape and normalize rgb to greyscale"""
    target_size=(224,224)
    img = keras_img.img_to_array(
            keras_img.load_img(fname, target_size=target_size)
    )
    img /= 255
    img = img.reshape((1, img.shape[0], img.shape[1], img.shape[2]))
    return img

def build_image_stack(fnames):
    """ Create np.array stack of all images for a syringe """
    
    data_array = np.empty((len(fnames),224,224,3),np.float32)
    for i, fname in enumerate(fnames):
        data_array[i,:,:,:] = prep_image(fname)

    return data_array


def get_fnames(class_dir):
    """ Return list of all class names
    Args:
        class_dir (str): directory to pull images from
    """
    return sorted(glob.glob(class_dir + "/*.png"))


def classify(model,data_array):
    """Run the classifier on the testing data"""
    output_array = model.predict(data_array)
    return output_array

def validate_model_path(path):
    """Check if model directory is correct.

    Args:
        path (str): Path to model directory
    """
    if not os.path.exists(path): 
        raise Exception("Unable to find model directory at {}".format(path))

def validate_training_set_path(path):
    """ Check that the path the training images (train/test/validate) is valid and raise an exception if it is not.
    Args:
        path (str): Path to training set data
    """
    if not os.path.exists(path):
        raise Exception("Unable to find the training directory at {}".format(path))

def main(args):
    gpus = tf.config.list_physical_devices('GPU')
    print(gpus)
    
    model_name = args.model_name.lower()

    if args.machine.lower() == 'hb':
        base_dir = "/hb/home/pcdaniel/ifcb-training/models/"
        training_dir = os.path.join(base_dir, args.training_set_path)
        fname_out_class = os.path.join("/hb/home/pcdaniel/ifcb-training/output/",model_name + "_class.csv")
        fname_out_name = os.path.join("/hb/home/pcdaniel/ifcb-training/output/",model_name + "_fnames.csv")
        validate_training_set_path(training_dir)
    
    elif args.machine.lower() == "dell":
        base_dir = "/home/pcdaniel/Documents/IFCB/ifcb-training/models/"
        training_dir = os.path.join("/home/pcdaniel/Documents/IFCB/image-extraction/", args.training_set_path)
        fname_out_class = os.path.join("/home/pcdaniel/Documents/IFCB/ifcb-training/output/",model_name + "_class.csv")
        fname_out_name = os.path.join("/home/pcdaniel/Documents/IFCB/ifcb-training/output/",model_name + "_fnames.csv")
        validate_training_set_path(training_dir)
    
    else:
        raise Exception("Unknown machine selected. Choose 'hb' or 'dell'")


    model_path = os.path.join(base_dir, model_name)
    validate_model_path(model_path)
    
    model = load_model(model_path)
    
    class_list = sorted(glob.glob(os.path.join(training_dir, "test/*")))
    class_list = [c for c in class_list if os.path.basename(c) != "UNUSED"]
    class_l = [os.path.basename(c) for c in class_list]
    all_fnames = np.array([])
    all_data = np.array([])


    for clss in class_list:
        fnames = get_fnames(clss)
        all_fnames = np.append(fnames,all_fnames)
        print("Classifying {} images of {}".format(len(fnames), clss))
        chunk_size = 10000

        if len(fnames) > chunk_size:
            n_chunks = np.ceil(len(fnames) / chunk_size)
            out_data = np.array([])
            print("Class {}: Too large. Chunking data".format(clss))
            
            for i in range(int(n_chunks)):
                print("Chunk {} of {} aka, {} to {}".format(i+1, int(n_chunks), i*chunk_size, (i+1)*chunk_size))
                if i != (int(n_chunks) - 1):
                    image_stack = build_image_stack(fnames=fnames[i*chunk_size:(i+1)*chunk_size])
                    out_data_chunk = classify(model, image_stack)

                else:
                    image_stack = build_image_stack(fnames=fnames[i*chunk_size:-1])
                    out_data_chunk = classify(model, image_stack)
                
                out_data = np.append(out_data_chunk,out_data)
                _ = gc.collect()  
                tf.keras.backend.clear_session()

        else:
            image_stack = build_image_stack(fnames)
            out_data = classify(model, image_stack)
            _ = gc.collect()  
            tf.keras.backend.clear_session()

        
        all_data = np.append(out_data, all_data)

    all_data = all_data.reshape(-1, len(class_l))
    df = pd.DataFrame(all_data, columns=class_l)
    df.to_csv(fname_out_class)
    
    df =  pd.DataFrame(all_fnames, columns=['fname'])
    df['base'] = [os.path.basename(d) for d in all_fnames]
    df['class'] = [d.split("/")[-2] for d in all_fnames]
    df.to_csv(fname_out_name)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = 'Provided a model name')
    parser.add_argument('--model_name', default="20230216-soft-1_5", type=str, required=False, help="Provide the name of the model to use. Assumes that the model is located in ./models.")
    parser.add_argument('--training_set_path',default="20230216_sorted", type=str, required=False, help="Choose 'Daily' or 'monthly'")
    parser.add_argument('--machine', default="dell", type=str, required=False, help="Choose machine: hb, dell")

    args = parser.parse_args()

    main(args)

    #python src/test-xception-hb.py --model_name 20230216-soft-none --training_set_path 20230216_sorted --machine dell