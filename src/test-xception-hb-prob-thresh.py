import tensorflow as tf
import keras_preprocessing.image as keras_img
import numpy as np
import glob
import pandas as pd
import os
import gc


def load_model(model_dir):
    """Load the xception model. This function is looking for the directory with the .pb file"""
    return tf.keras.models.load_model(model_dir)


def prep_image(fname):
    """Load and prep images for model, reshape and normalize rgb to greyscale"""
    target_size=(224,224)
    img = keras_img.img_to_array(
            keras_img.load_img(fname, target_size=target_size)
    )
    img /= 255
    img = img.reshape((1, img.shape[0], img.shape[1], img.shape[2]))
    return img

def build_image_stack(fnames):
    """ Create np.array stack of all images for a syringe """
    
    data_array = np.empty((len(fnames),224,224,3),np.float32)
    for i, fname in enumerate(fnames):
        data_array[i,:,:,:] = prep_image(fname)

    return data_array


def get_fnames(class_dir):
    """ Return list of all class names
    Args:
        class_dir (str): directory to pull images from
    """
    return sorted(glob.glob(class_dir + "/*.png"))


def classify(model,data_array):
    """Run the classifier on the testing data"""
    output_array = model.predict(data_array)
    return output_array


if __name__ == "__main__":
    gpus = tf.config.list_physical_devices('GPU')

    model = load_model("./models/20230216-soft-none/")
    class_list = sorted(glob.glob("/home/pcdaniel/Documents/IFCB/image-extraction/20230216_sorted/test/*"))
    class_l = [os.path.basename(c) for c in class_list]

    
    all_fnames = np.array([])
    all_data = np.array([])

    fnames = sorted(glob.glob("/home/pcdaniel/Documents/IFCB/image-extraction/prob_threshold_unknowns/*.png"))
    print("Classifying {} images".format(len(fnames)))
    chunk_size = 500

    if len(fnames) > chunk_size:
        n_chunks = np.ceil(len(fnames) / chunk_size)
        out_data = np.array([])
        print("Too large. Chunking data")
        
        for i in range(int(n_chunks)):
            print("Chunk {} of {} aka, {} to {}".format(i+1, int(n_chunks), i*chunk_size, (i+1)*chunk_size))
            if i != (int(n_chunks) - 1):
                image_stack = build_image_stack(fnames=fnames[i*chunk_size:(i+1)*chunk_size])
                out_data_chunk = classify(model, image_stack)

            else:
                image_stack = build_image_stack(fnames=fnames[i*chunk_size:-1])
                out_data_chunk = classify(model, image_stack)
            
            out_data = np.append(out_data_chunk,out_data)
            _ = gc.collect()  
            tf.keras.backend.clear_session()

    else:
        image_stack = build_image_stack(fnames)
        out_data = classify(model, image_stack)
        _ = gc.collect()  
        tf.keras.backend.clear_session()

    
    all_data = np.append(out_data, all_data)

    all_data = all_data.reshape(-1, 51)
    df = pd.DataFrame(all_data, columns=class_l)
    df['fname'] = [os.path.basename(d) for d in fnames[:-1]]
    df.to_csv("/home/pcdaniel/Documents/IFCB/ifcb-training/output/nonclassified_output_soft_none.csv")