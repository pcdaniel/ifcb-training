import tensorflow as tf
import numpy as np
import configparser
from tensorflow.keras.applications import Xception
from tensorflow.keras.layers import Dense, Lambda
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint
from tensorflow.keras import mixed_precision
import tensorboard
# from tensorflow.keras.initializers import Constant

import keras.backend as K

import datetime as dt
import json
import glob
import os
import time


print(os.environ["CUDA_VISIBLE_DEVICES"])
os.environ["CUDA_VISIBLE_DEVICES"]="0"


def create_data_generators(path_to_training, path_to_validation, batch_size=200, target_size=(224, 224), save_label=False):
    """ Create training and validation Image dateset generators. A lot of choices are made here, so use caution and take notes!

    Args:
        path_to_training (str): directory to the training data. Data should be organized as images in directories, where the directory name is the class label
        path_to_validation (str): Same as validation data. Training, Validation, and Testing datasets were split 8:1:1 using a notebook found in the notebook dir
        batch_size (int, optional): The number of images to be chunked up. Defaults to 200. Can probably be much higher on Hummingbird.
        target_size (tuple, optional): _description_. Defaults to (224, 224).
        save_label (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """
    test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255)
    train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
        horizontal_flip=True,
        vertical_flip=True
    )
    train_generator = train_datagen.flow_from_directory(
        path_to_training,
        target_size=target_size,
        batch_size=batch_size,
        class_mode='categorical'
    )
    validation_generator = test_datagen.flow_from_directory(
        path_to_validation,
        target_size=target_size,
        batch_size=batch_size,
        class_mode='categorical'
    )

    if save_label:
        with open( os.path.join("/".join(path_to_training.split("/")[:-2]),"xception-class-label.json") , "w") as outfile:
            json.dump(train_generator.class_indices, outfile)

    return train_generator, validation_generator


def create_model(number_of_classes,softmax_scale):
    """Generate Xception Model Instance and compile it.

    There are a lot of choices to make here. We want to use the pre-trained weights from imagenet, include_top --> whether to include the fully-connected layer at the top of the network, pooling="avg" --> reduces the size of the feature map, there is an option to use "max" values instead if gradient detection is more important (I think)

    Returns:
       [tf.keras.Model] : Compiled model object.
    """
    base_model = Xception(
        weights='imagenet',
        include_top=False,
        pooling='avg'
    )

    # Add new fully connected layers for ifcb
    # dense layer to classify with softmax
    model_output = base_model.output
    model_output = Dense(1024, activation='relu',name='layer_relu')(model_output)
    model_output = base_model.output
    trainable_layers = 2

    if softmax_scale is not None:
        model_output = Lambda(lambda x: x * np.log(float(softmax_scale)),name='layer_lambda_scale')(model_output)
        trainable_layers = 3

    model_output = Dense(number_of_classes, activation='softmax',name='layer_softmax')(model_output) # Note this has to be the number of the labels in the dataset
    model = Model(inputs=base_model.input, outputs=model_output)

    # Only train the dense layers, user transfer learning for the rest
    for layer in model.layers[:-trainable_layers]:
        layer.trainable = False
    for layer in model.layers[-trainable_layers::]:
        layer.trainable = True

    model.compile(
        optimizer='Adam',
        loss='categorical_crossentropy',
        metrics=['categorical_accuracy', tf.keras.metrics.AUC()],
    )

    return model


def train_model(model, train_generator, validation_generator, save_dir, batch_size, epochs):
    """Train model from training and validation dataset generators.

    Args:
        model (_type_): _description_
        train_generator (_type_): _description_
        validation_generator (_type_): _description_
        save_dir (_type_): _description_
        batch_size (_type_): _description_
    """
    checkpoint_cb = ModelCheckpoint(
        filepath=save_dir,
        save_weights_only=False,
        monitor='loss',
        mode='min',
        save_best_only=True
    )
    # Reduces learning rate when learning begins to plateau between epochs
    # factor = fraction to reduce learning
    # patience = number of epochs to monitor for reduction
    # min_lr = lower bound of learning rate
    learning_rate_cb = ReduceLROnPlateau(
        monitor='loss',
        factor=0.3,
        patience=2,
        min_lr=0.0001
    )
    stop_early_cb = EarlyStopping(
        monitor='loss',
        min_delta=0,
        patience=2,
    )

    log_dir = os.path.join( save_dir, "logs/" + dt.datetime.now().strftime("%Y%m%d-%H%M%S"))
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    with tf.device('/device:GPU:0'):
        model.fit(
            train_generator,
            epochs=epochs,
            steps_per_epoch=train_generator.samples // batch_size,
            validation_data=validation_generator,
            validation_freq=validation_generator.samples // batch_size,
            callbacks=[checkpoint_cb, learning_rate_cb, stop_early_cb, tensorboard_callback]
        )
        
    model.save(save_dir)


if __name__ == "__main__":
    start_timer = time.perf_counter() # We need to time the code to see if multiprocessing is actually helping; this starts the timer
    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))

    config = configparser.ConfigParser()
    config.read("./experiments/humming.ini")
    BASE_DIR = config['train_config']['data_dir']
        
    
    train_path = os.path.join(BASE_DIR, "train/")
    val_path = os.path.join(BASE_DIR, "validate/")
    model_out_path = config['train_config']['model_dir']

    print("Building dataset generator")
    batch_size = int(config['train_config']['batch_size'])
    epochs = int(config['train_config']['epochs'])
    number_of_classes = len(glob.glob(os.path.join(train_path,"*")))
    print("Number of Classes: {}".format(number_of_classes))
    train_gen, val_gen = create_data_generators(train_path, val_path, batch_size=batch_size, save_label=True)

    print("Building Model")
    softmax_scale = config['train_config']['softmax_scale']
    if softmax_scale.lower() == "none":
        softmax_scale = None
    model = create_model(number_of_classes=number_of_classes, softmax_scale=softmax_scale)

    print("Training Model")
    train_model(model, train_gen, val_gen, model_out_path, batch_size=batch_size, epochs=epochs)
    finish_timer = time.perf_counter()
    print(f'Finished Training in {round(finish_timer-start_timer,2 )} second(s)')