# IFCB Training Scripts #
Author: Patrick Daniel (pcdaniel@ucsc.edu)

This project is to train CNN using an human-annotated training set of phytoplankton images from the Imaging Flow Cytobots deployed by the Kudela Lab. The training set has been meticulously developed by various members of the lab and is around ~300,000 images.

This codebase was developed during the EART219 UCSC course.

### Data Used
The model is trained using a dataset of manually classified images of phytoplankton taken from the Santa Cruz pier. Images are sorted into directories based on the label.  
 ![](./centric_example.png)  
 Example: Centric diatom.

## Dealing Class Imbalance
Undersample: classes with over 100,000 images are randomly undersampled to 100,000  
Oversampling: Select classes with fewer than 150 samples are randomly oversampled by pulling images and rotating and flipping them. These image are labled with the synthetic prefix.
Softmax: There is an optional place to alter the softmax activation layer. A float is passed to an extra layer before softmax activation that will scale the values as the natural log of the value passed. Hence passing _e_ would result in a multiplcation by 1. This is following `Kraft et al 2022`.

## Training the Model

The model is trained using the Xception framework using the TensorFlow package. This should be done using a machine utilizing at GPU. Here we want to use the hummingbird compute cluster GPU-Node to train the model.  

Images need to be split into a testing, training, and validation, based on a 60:20:20 percent split. See below for explanation.


### Running on HummingBird ##

Programs are run on the cluster using the Slurm workload manager. This can be done easily and efficiently using a SLURM script (.slurm).

Setup a SLURM script - see examples at /hb/software/scripts/gpu-example.slurm  

Running `sbatch training-ifcb.slurm`


## Testing the Model
Once the model is completed, we need to evaluate how well the model works on __images the model has never seen__. These images will get classified using the trained model and some statistics can be calculated to assess how well the model works.

Testing has been updated to run on the lab workstation. Here is an example call:

`python src/test-xception-hb.py --model_name 20230216-soft-none --training_set_path 20230216_sorted --machine dell`

~Run using `sbatch testing-ifcb.slurm`~

### Results! ###

Results statistics were calculated in the [7class-model-robustness.ipynb](./notebooks/27class-model-robustness.ipynb). F1-score, Precision, and Recall (accuracy) were all estimated for the all data and for each class.

![](./notebooks/figures/testing-results.png)  

The heatmap shows the fraction of correctly classified Images by class. The y-axis is the true value and the x-axis is the model classified result. Read each row left to right.

![](./notebooks/figures/27-class-prediction-matrix.png)


## Data Exploration Notebooks

There are a couple of scripts for moving around the data and setting up for training a model.   

- [count-images.ipynb](./notebooks/count-images.ipynb): Count the number of images in each of the training classes. The manual classification data was accessed on 2.21.22.
![](./notebooks/figures/class_count.png)

- [sort-images-by-role.ipynb](./notebooks/sort-images-by-role.ipynb): This notebook assigns a role to each image within a class based on the distribution given.

  __Training (80%)__: These are used for training the model  
  __Validation (10%)__: These are how the model validates its accuracy  
  __Testing (10%)__: The model never sees these data and they can be used to test robustness  

  Since species distribution is not random, we want to ensure that the 8:1:1 is maintained for each class. This is done by randomizing the entire dataframe and then labeling the the first 80% as training, next 10% as validation and the rest as testing. The thought process for this is that they may be some differences between images collected on different and we want to try to capture that.
